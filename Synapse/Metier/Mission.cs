﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synapse.Metier
{
    class Mission
    {
        private string _nom;
        private string _description;
        private int _nbHeuresPrevues;
        private Dictionary <DateTime, int> _releveHoraire;

        private string Nom
        {
            get { return _nom; }
            set { _nom = value; }
        }

        private string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private int NbHeuresPrevues
        {
            get { return _nbHeuresPrevues; }
            set { _nbHeuresPrevues = value; }
        }

        private Dictionary <DateTime, int> ReleveHoraires
        {
            get { return _releveHoraire; }
            set { _releveHoraire = value; }
        }

        public void AjouteReleve(DateTime date, int nbHeures)
        {
            _releveHoraire.Add(date, nbHeures);
        }

        public void NbHeuresEffectuees()
        {
            int nbHeures = 0;
            foreach (Dictionary<DateTime, int> releveHoraire in _releveHoraire)
            {
                nbHeures += releveHoraire.Values;
            }
            return nbHeures;
        }
    }
}
