﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synapse.Metier
{
    class Projet
    {
        private string _nom;
        private DateTime _debut;
        private DateTime _fin;
        private decimal _prixFactureMo;

        private string Nom
        {
            get { return _nom; }
            set { _nom = value; }
        }

        private DateTime Debut
        {
            get { return _debut; }
            set { _debut = value; }
        }

        private DateTime Fin
        {
            get { return _fin; }
            set { _fin = value; }
        }

        private decimal PrixFactureMO
        {
            get { return _prixFactureMo; }
            set { _prixFactureMo = value; }
        }

        private decimal CumulCoutMo()
        {
            decimal cumul = 0;
            foreach (Mission m in this._missions)
            {
                cumul += m.nbHeuresEffectuees() * m.getExcecutant().getTauxHoraire();
            }
            return cumul;
        }

        private decimal MargeBrutCourante()
        {
            decimal marge = 0;
            marge = this.cumul - this.PrixFactureMO; 
            return marge;
        }
    }
}
