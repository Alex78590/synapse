﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synapse.Metier
{
    class Intervenant
    {
        private string _nom;
        private decimal _tauxHoraire;

        private string Nom
        {
            get { return _nom; }
            set { _nom = value; }
        }

        private decimal TauxHoraire
        {
            get { return _tauxHoraire; } 
            set { _tauxHoraire = value; }
        }

    }
}
